import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import { AddressBookModuleStore } from './store';

export const AddressBookModule: StorefrontModule = function ({
  app,
  store,
  router,
  moduleConfig,
  appConfig
}) {
  store.registerModule('addressBook', AddressBookModuleStore)
};
