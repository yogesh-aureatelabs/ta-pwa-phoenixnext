export default interface Attributes {
  attribute_code: string,
  value: string
}
