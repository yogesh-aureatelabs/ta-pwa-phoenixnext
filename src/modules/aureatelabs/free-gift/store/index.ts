import { Module } from 'vuex'
import FreeGiftState from '../types/FreeGiftState'
import actions from './actions'
import { mutations } from './mutations'
import { getters } from './getters'
export const FreeGiftModuleStore: Module<FreeGiftState, any> = {
  namespaced: true,
  state: {
    freegifts: []
  },
  mutations,
  actions,
  getters
}
