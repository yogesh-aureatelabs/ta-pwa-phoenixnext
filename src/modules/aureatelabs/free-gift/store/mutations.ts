import { MutationTree } from 'vuex'
import * as types from './mutation-types'

export const mutations: MutationTree<any> = {
  [types.FREE_GIFT_FETCH] (state, freeGift) {
    state.freegifts = freeGift
  }
}
