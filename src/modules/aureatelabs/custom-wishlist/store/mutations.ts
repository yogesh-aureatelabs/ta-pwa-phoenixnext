import { MutationTree } from 'vuex'
import * as types from './mutation-types'

export const mutations: MutationTree<any> = {
  [types.CUSTOM_WISHLIST_DATA] (state, customwishlist) {
    state.customwishlist = customwishlist || []
  },
  [types.WISHLIST_DATA_LIST] (state, wishlistdatalist) {
    state.wishlistdatalist = wishlistdatalist || []
  }
}
