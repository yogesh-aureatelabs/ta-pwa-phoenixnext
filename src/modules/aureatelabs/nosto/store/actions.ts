import Vue from 'vue'
import { ActionTree } from 'vuex';
import NostoState from '../types/NostoState'
import NostoRecommendationState from '../types/NostoRecommendationState'
import * as types from './mutation-types';
import { prepareQuery } from '@vue-storefront/core/modules/catalog/queries/common';
declare const window: any;
const actions: ActionTree<NostoState, any> = {
  list (context, recommendationIds: NostoRecommendationState) {
    let prepareArray = {};
    if (typeof window !== 'undefined' && typeof window.nostojs !== 'undefined') {
      window.nostojs(api => {
        api.defaultSession()
          .viewFrontPage()
          .setPlacements(recommendationIds)
          .load()
          .then(response => {
            context.commit(types.FETCH_ALL_NOSTOLIST, response.recommendations);
            for (const [key, value] of Object.entries(response.recommendations)) {
              if (value['result_type'] === 'REAL') {
                var newArray = [];
                for (const [divId, divValue] of Object.entries(value['products'])) {
                  let pid = value['products'][divId].product_id;
                  newArray.push(pid);
                }
                prepareArray[key] = newArray
              }
            }
            context.commit(types.FETCH_ALL_RECOMMEND_PRODUCTS, prepareArray);
            context.dispatch('getProductList', prepareArray)
            return prepareArray
          }).catch(error => {
            console.error('There was an error!', error);
          });
      });
    }
  },
  async getProductList ({ state, rootState, dispatch, commit }, productIdsList) {
    try {
      let resultArray = {};
      for (const [divId, productIds] of Object.entries(productIdsList)) {
        let newProductsQuery = prepareQuery({ filters: [{ key: 'sku', value: { 'in': productIds } }] });
        const newProductsResult = await dispatch('product/list', { query: newProductsQuery, skipCache: true, size: 500 },
          {
            root: true
          }).then(res => {
          return res.items;
        }).catch(error => {
          console.error('There was an error!', error);
        });
        resultArray[divId] = newProductsResult
      }
      commit(types.FETCH_ALL_RECOMMENDATIONS, resultArray);
    } catch (error) {
      console.log(error)
    }
  },
  sendOrderData (context, nostoData) {
    if (typeof window !== 'undefined' && typeof window.nostojs !== 'undefined') {
      window.nostojs(api => {
        api.defaultSession()
          .addOrder(nostoData)
          .setPlacements(['order-related'])
          .load()
          .then(data => {
            console.log('Data Recommendations: ' + data.recommendations);
          }).catch(error => {
            console.error('There was an error!', error);
          })
      })
    }
  },
  viewFrontPage () {
    if (typeof window !== 'undefined' && typeof window.nostojs !== 'undefined') {
      window.nostojs(api => {
        api.defaultSession()
          .viewFrontPage()
          .setPlacements([
            'frontpage-nosto-most-popular',
            'frontpage-nosto-trending-products',
            'frontpage-nosto-featured-products',
            'frontpage-nosto-recommended-for-you',
            'frontpage-nosto-just-arrived',
            'frontpage-nosto-best-picked-products',
            'frontpage-nosto-best-sellers',
            'frontpage-nosto-selection-products',
            'frontpage-nosto-history-related',
            'frontpage-nosto-history'
          ])
          .load()
          .then(data => {
            console.log(data.recommendations);
          })
      })
    }
  },
  viewProduct (context, id) {
    if (typeof window !== 'undefined' && typeof window.nostojs !== 'undefined') {
      window.nostojs(api => {
        api.defaultSession()
          .viewProduct(id)
          .setPlacements([
            'frontpage-nosto-history',
            'frontpage-nosto-history-related'
          ])
          .load()
          .then(data => {
            console.log(data.recommendations);
          })
      })
    }
  },
  viewCategory (context, category) {
    if (typeof window !== 'undefined' && typeof window.nostojs !== 'undefined') {
      window.nostojs(api => {
        api.defaultSession()
          .viewCategory(category)
          .setPlacements(['category-related'])
          .load()
          .then(data => {
            console.log(data.recommendations);
          })
      })
    }
  },
  viewSearch (context, query) {
    if (typeof window !== 'undefined' && typeof window.nostojs !== 'undefined') {
      window.nostojs(api => {
        api.defaultSession()
          .viewSearch(query)
          .setPlacements([
            'search-related',
            'frontpage-nosto-selection-products',
            'frontpage-nosto-trending-products'
          ])
          .load()
          .then(data => {
            console.log(data.recommendations);
          })
      })
    }
  },
  viewCart () {
    if (typeof window !== 'undefined' && typeof window.nostojs !== 'undefined') {
      window.nostojs(api => {
        api.defaultSession()
          .viewCart()
          .setPlacements([
            'cart-related',
            'frontpage-nosto-most-popular',
            'frontpage-nosto-trending-products',
            'frontpage-nosto-featured-products',
            'frontpage-nosto-best-picked-products',
            'frontpage-nosto-best-sellers'
          ])
          .load()
          .then(data => {
            console.log(data.recommendations);
          })
      })
    }
  },
  reportAddToCart (context, sku) {
    if (typeof window !== 'undefined' && typeof window.nostojs !== 'undefined') {
      window.nostojs(api => {
        api.defaultSession()
          .reportAddToCart(sku, 'element')
          .update()
      })
    }
  }
}

export default actions
