import { MutationTree } from 'vuex'
import * as types from './mutation-types'
import AuthState from '../types/AuthState'

const mutations: MutationTree<AuthState> = {
  [types.SET_LOGIN_USER] (state, user) {
    state.loginUser = user
  }
}
export default mutations
