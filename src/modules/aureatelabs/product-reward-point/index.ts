import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import { ProductRewardPointsModuleStore } from './store/product-reward-point/index';

export const ProductRewardPointsModule: StorefrontModule = function ({
  app,
  store,
  router,
  moduleConfig,
  appConfig
}) {
  store.registerModule('productRewardPoints', ProductRewardPointsModuleStore)
};
