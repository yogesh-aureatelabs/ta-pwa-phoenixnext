import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import { RewardPointsModuleStore } from './store/reward-points/index';

export const RewardPointsModule: StorefrontModule = function ({
  app,
  store,
  router,
  moduleConfig,
  appConfig
}) {
  store.registerModule('rewardPoints', RewardPointsModuleStore)
};
