import Vue from 'vue'
import { ActionTree } from 'vuex';
import RewardPointsState from '../../types/RewardPointsState';
import rootStore from '@vue-storefront/core/store'
import * as types from './mutation-types'
import { StorageManager } from '@vue-storefront/core/lib/storage-manager'

const actions: ActionTree<RewardPointsState, any> = {
  async list (context) {
    let url = rootStore.state.config.api.url + rootStore.state.config.aureatelabs.aureate_rewards;
    const usersCollection = StorageManager.get('user')
    const userData = await usersCollection.getItem('current-user')

    try {
      await fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ u_id: userData.id })
      }).then(res => res.json()).then((resp) => {
        context.commit(types.REWARD_FETCH_REWARD, resp);
      });
    } catch (e) {
      console.log(e.message)
    }
  },
  async addRewardPoints (context, parameters) {
    let url = rootStore.state.config.api.url + rootStore.state.config.aureatelabs.aureate_apply_reward_points;
    let rewardPoint = parameters[0]
    let cartId = parameters[1]

    try {
      let response = await fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ rewardPoint: rewardPoint, cartId: cartId })
      }).then(res => res.json()).then((resp) => {
        context.commit(types.REWARD_APPLIED_REWARD, resp);
        return resp;
      })
      return response
    } catch (e) {
      console.log(e.message)
    }
  }
}

export default actions
