import { Module } from 'vuex'
import RewardPointsState from '../../types/RewardPointsState'
import actions from './actions'
import { mutations } from './mutations'
import { getters } from './getters'
export const RewardPointsModuleStore: Module<RewardPointsState, any> = {
  namespaced: true,
  state: {
    rewardPoints: [],
    appliedRewardPoints: []
  },
  mutations,
  actions,
  getters
}
