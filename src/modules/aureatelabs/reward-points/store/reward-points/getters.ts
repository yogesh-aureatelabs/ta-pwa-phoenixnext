import RewardPointsState from '../../types/RewardPointsState'
import { GetterTree } from 'vuex';

export const getters: GetterTree<RewardPointsState, any> = {
  getRewards: (state) => state.rewardPoints,
  getAppliedRewards: (state) => state.appliedRewardPoints
}
