import Vue from 'vue'
import { ActionTree } from 'vuex';
import PaymentState from '../types/PaymentState'
import rootStore from '@vue-storefront/core/store'
import i18n from '@vue-storefront/i18n'
import Payment from '../types/Payment';
import { Base64 } from 'js-base64';

var crypto = require('crypto');

const actions: ActionTree<PaymentState, any> = {
  processPayment ({ dispatch }, PaymentData: Payment) {
    function formatPrice (price) {
      var my_string = '' + Number(price).toFixed(2).replace('.', '');
      while (my_string.length < 12) {
        my_string = '0' + my_string;
      }
      return my_string;
    }
    var merchantID = rootStore.state.config.aureatelabs.payment.merchantID;
    var secretKey = rootStore.state.config.aureatelabs.payment.secretKey;
    var desc = rootStore.state.config.aureatelabs.payment.desc;
    var uniqueTransactionCode = Date.now();
    var currencyCode = rootStore.state.config.aureatelabs.payment.currencyCode;
    var amt = formatPrice(rootStore.state.cart.platformTotals.grand_total);
    var storeCard = 'Y';
    // var amt = formatPrice(rootStore.getters['cart/getTotals'][Object.keys(rootStore.getters['cart/getTotals']).length - 1].value);
    var panCountry = rootStore.state.config.aureatelabs.payment.panCountry;
    // Customer Information
    var cardholderName = rootStore.getters['checkout/getPersonalDetails'].firstName + ' ' + rootStore.getters['checkout/getPersonalDetails'].lastName;
    var version = rootStore.state.config.aureatelabs.payment.version;

    if (PaymentData.paymentToken !== undefined) {
      var xml = '<PaymentRequest>'
      xml += '<merchantID>' + merchantID + '</merchantID>';
      xml += '<uniqueTransactionCode>' + uniqueTransactionCode + '</uniqueTransactionCode>';
      xml += '<desc>' + desc + '</desc>';
      xml += '<amt>' + amt + '</amt>';
      xml += '<currencyCode>' + currencyCode + '</currencyCode>';
      xml += '<storeCardUniqueID>' + PaymentData.paymentToken + '</storeCardUniqueID>';
      xml += '<panCountry>' + panCountry + '</panCountry>';
      xml += '<cardholderName>' + PaymentData.cardholdername + '</cardholderName>';
      xml += '<encCardData></encCardData>';
      xml += '<storeCard>' + storeCard + '</storeCard>';
      xml += '</PaymentRequest>';
      var paymentPayload = Base64.encode(xml);
    } else {
      var xml = '<PaymentRequest>'
      xml += '<merchantID>' + merchantID + '</merchantID>';
      xml += '<uniqueTransactionCode>' + uniqueTransactionCode + '</uniqueTransactionCode>';
      xml += '<desc>' + desc + '</desc>';
      xml += '<amt>' + amt + '</amt>';
      xml += '<currencyCode>' + currencyCode + '</currencyCode>';
      xml += '<panCountry>' + panCountry + '</panCountry>';
      xml += '<cardholderName>' + PaymentData.cardholdername + '</cardholderName>';
      xml += '<encCardData>' + PaymentData.encryptedCardInfo + '</encCardData>';
      xml += '</PaymentRequest>';
      var paymentPayload = Base64.encode(xml);
    }

    var signature = crypto.createHmac('sha256', secretKey).update(paymentPayload).digest('hex').toUpperCase();
    var payloadXML = '<PaymentRequest>';
    payloadXML += '<version>' + version + '</version>';
    payloadXML += '<payload>' + paymentPayload + '</payload>';
    payloadXML += '<signature>' + signature + '</signature>';
    payloadXML += '</PaymentRequest>';
    var payload = Base64.encode(payloadXML);
    dispatch('submitPayment', payload)
  },
  processQRPayment ({ dispatch }) {
    function formatPrice (price) {
      var my_string = '' + Number(price).toFixed(2).replace('.', '');
      while (my_string.length < 12) {
        my_string = '0' + my_string;
      }
      return my_string;
    }
    var merchantID = rootStore.state.config.aureatelabs.payment.merchantID;
    var secretKey = rootStore.state.config.aureatelabs.payment.secretKey;

    // Transaction Information
    var desc = rootStore.state.config.aureatelabs.payment.desc;
    var uniqueTransactionCode = Date.now();
    var currencyCode = rootStore.state.config.aureatelabs.payment.currencyCode;
    var amt = formatPrice(rootStore.state.cart.platformTotals.grand_total);
    // var amt = formatPrice(rootStore.getters['cart/getTotals'][Object.keys(rootStore.getters['cart/getTotals']).length - 1].value);
    var panCountry = rootStore.state.config.aureatelabs.payment.panCountry; // Not required for APM payment

    // Customer Information
    var cardholderName = rootStore.getters['checkout/getPersonalDetails'].firstName + ' ' + rootStore.getters['checkout/getPersonalDetails'].lastName; // Not required for APM payment

    /** NOT REQUIRED FOR APM PAYMENT
    //Encrypted card data
    $encCardData = $_POST['encryptedCardInfo'];

    //Retrieve card information for merchant use if needed
    $maskedCardNo = $_POST['maskedCardInfo'];
    $expMonth = $_POST['expMonthCardInfo'];
    $expYear = $_POST['expYearCardInfo'];
    **/

    // Payment Options
    var paymentChannel = rootStore.state.config.aureatelabs.payment.paymentChannel; // Set transaction as Alternative Payment Method
    var agentCode = rootStore.state.config.aureatelabs.payment.agentCode; // APM agent code
    var channelCode = rootStore.state.config.aureatelabs.payment.channelCode; // APM channel code
    let date = new Date();
    var paymentExpiry = (new Date()).toISOString().split('T')[0] + ' 23:59:59'; // pay slip expiry date (optional). format yyyy-MM-dd HH:mm:ss
    var mobileNo = rootStore.state.checkout.shippingDetails.phoneNumber;// customer mobile number
    var cardholderEmail = rootStore.getters['checkout/getPersonalDetails'].emailAddress; // customer email address

    // Request Information
    var version = '9.9';

    // Construct payment request message
    var xml = '<PaymentRequest>';
    xml += '<merchantID>' + merchantID + '</merchantID>';
    xml += '<uniqueTransactionCode>' + uniqueTransactionCode + '</uniqueTransactionCode>';
    xml += '<desc>' + desc + '</desc>';
    xml += '<amt>' + amt + '</amt>';
    xml += '<currencyCode>' + currencyCode + '</currencyCode>';
    xml += '<panCountry>' + panCountry + '</panCountry>';
    xml += '<cardholderName>' + cardholderName + '</cardholderName>';
    xml += '<paymentChannel>' + paymentChannel + '</paymentChannel>';
    xml += '<agentCode>' + agentCode + '</agentCode>';
    xml += '<channelCode>' + channelCode + '</channelCode>';
    xml += '<paymentExpiry>' + paymentExpiry + '</paymentExpiry>';
    xml += '<mobileNo>' + mobileNo + '</mobileNo>';
    xml += '<cardholderEmail>' + cardholderEmail + '</cardholderEmail>';
    xml += '<encCardData></encCardData>';
    xml += '</PaymentRequest>';
    var paymentPayload = Base64.encode(xml); // Convert payload to base64

    var signature = crypto.createHmac('sha256', secretKey).update(paymentPayload).digest('hex').toUpperCase();
    var payloadXML = '<PaymentRequest>';
    payloadXML += '<version>' + version + '</version>';
    payloadXML += '<payload>' + paymentPayload + '</payload>';
    payloadXML += '<signature>' + signature + '</signature>';
    payloadXML += '</PaymentRequest>';
    var payload = Base64.encode(payloadXML);
    dispatch('submitPayment', payload)
  },
  submitPayment (context, payload) {
    var form = document.createElement('form');
    var element1 = document.createElement('input');

    form.method = 'POST';
    form.action = rootStore.state.config.aureatelabs.payment.action;

    element1.value = payload;
    element1.name = 'paymentRequest';
    form.appendChild(element1);

    document.body.appendChild(form);

    form.submit();
  }

}
export default actions
