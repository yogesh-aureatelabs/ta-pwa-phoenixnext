export default interface Payment {
  cardnumber: any[],
  month: any,
  year: any,
  cvv: any,
  encryptedCardInfo: any,
  maskedCardInfo: any,
  expMonthCardInfo: any,
  expYearCardInfo: any,
  paymentToken: any,
  cardholdername: any
}
