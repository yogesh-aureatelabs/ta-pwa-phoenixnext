import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import { BrandsModuleStore } from './store/index';

export const BrandsModule: StorefrontModule = function ({
  store
}) {
  store.registerModule('brands', BrandsModuleStore)
};
