import { Module } from 'vuex'
import ContactState from '../types/ContactState'
import actions from './actions'

export const contactUs: Module<ContactState, any> = {
  namespaced: true,
  state: {
    items: []
  },
  actions
}
